use std::sync::Arc;

use anyhow::{bail, Result};
use tokio::{fs, io::AsyncReadExt};
use tokio_rustls::{rustls, TlsAcceptor};

pub async fn new_acceptor(certs_path: &str, private_key_path: &str) -> Result<TlsAcceptor> {
    let certs = load_certs(certs_path).await?;
    let private_key = load_private_key(private_key_path).await?;
    let mut tls_conf = rustls::ServerConfig::new(rustls::NoClientAuth::new());
    tls_conf.set_single_cert(certs, private_key)?;
    Ok(TlsAcceptor::from(Arc::new(tls_conf)))
}

async fn load_certs(filename: &str) -> Result<Vec<rustls::Certificate>> {
    // rustls_pemfile insists on using a std::io::BufReader as input, so this
    // does an async read to memory first.
    let mut contents = vec![];
    fs::File::open(filename)
        .await?
        .read_to_end(&mut contents)
        .await?;
    Ok(rustls_pemfile::certs(&mut (&contents as &[u8]))
        .unwrap()
        .iter()
        .map(|v| rustls::Certificate(v.clone()))
        .collect())
}

async fn load_private_key(filename: &str) -> Result<rustls::PrivateKey> {
    let mut contents = vec![];
    fs::File::open(filename)
        .await?
        .read_to_end(&mut contents)
        .await?;

    loop {
        match rustls_pemfile::read_one(&mut (&contents as &[u8]))? {
            Some(rustls_pemfile::Item::RSAKey(key)) => return Ok(rustls::PrivateKey(key)),
            Some(rustls_pemfile::Item::PKCS8Key(key)) => return Ok(rustls::PrivateKey(key)),
            None => break,
            _ => {}
        }
    }

    bail!(
        "no keys found in {:?} (encrypted keys not supported)",
        filename
    );
}
