use argh::FromArgs;

#[derive(FromArgs)]
/// Privacy-preserving giphy API proxy.
pub struct Args {
    /// path to the TLS certificate file
    #[argh(option)]
    pub tls_cert_path: String,
    /// path to the TLS private key file
    #[argh(option)]
    pub tls_private_key_path: String,
    /// address:port to listen on
    #[argh(option, default = "String::from(\"127.0.0.1:8080\")")]
    pub listen_addr: String,
    /// comma-separated list of domains to proxy for
    #[argh(option, default = "String::from(\"api.giphy.com\")")]
    pub proxy_domains: String,
}
