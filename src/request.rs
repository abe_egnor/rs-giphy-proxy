use std::{collections::HashSet, sync::Arc};

use anyhow::{bail, Result};
use httparse::Request;
use tokio::{
    io::{self, AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt},
    net::TcpStream,
};
use tokio_rustls::{server::TlsStream, TlsAcceptor};

pub struct Handler {
    pub acceptor: TlsAcceptor,
    pub domains: Arc<HashSet<String>>,
}

impl Handler {
    pub async fn run(&self, stream: TcpStream) -> Result<()> {
        let mut stream = self.acceptor.accept(stream).await?;
        let (host, port) = parse_request(&mut stream).await?;

        if !self.domains.contains(&host) {
            log::info!("invalid host: {:?}", host);
            // TODO: what's the right error code here?
            stream
                .write_all(b"HTTP/1.1 421 MISDIRECTED REQUEST\r\n\r\n")
                .await?;
            stream.shutdown().await?;
            return Ok(());
        }

        // DNS lookup is potentially blocking, so put it on the blocking pool.
        log::info!("starting DNS lookup");
        let giphy_addrs =
            tokio::task::spawn_blocking(|| dns_lookup::lookup_host("api.giphy.com")).await??;
        log::info!("DNS resolved to {:?}", giphy_addrs);
        if giphy_addrs.is_empty() {
            bail!("No results returned for api.giphy.com resolution");
        }

        stream.write_all(b"HTTP/1.1 200 OK\r\n\r\n").await?;
        stream.flush().await?;

        let giphy_stream = TcpStream::connect((giphy_addrs[0], port)).await?;
        let (mut client_read, mut client_write) = io::split(stream);
        let (mut giphy_read, mut giphy_write) = io::split(giphy_stream);
        log::info!("TCP proxying started");
        tokio::task::spawn(async move {
            let result = copy_and_log("client -> giphy", &mut client_read, &mut giphy_write).await;
            log::info!("client -> giphy done");
            result
        });
        tokio::task::spawn(async move {
            let result = copy_and_log("giphy -> client", &mut giphy_read, &mut client_write).await;
            log::info!("giphy -> client done");
            result
        });
        Ok(())
    }
}

async fn parse_request(stream: &mut TlsStream<TcpStream>) -> Result<(String, u16)> {
    let mut buffer = vec![];
    // For N iterations, this causes the total number of parses to be N+1.
    // This feels like it should be fixable, but the request is bound to the
    // lifetime of the buffer, so the buffer can't be extended if it's been
    // used in a parse.
    log::info!("waiting for request");
    // TODO: timeout
    while is_partial_request(&buffer)? {
        let mut read_buffer = [0; 1024];
        let n = stream.read(&mut read_buffer).await?;
        if n == 0 {
            bail!("socket closed");
        }
        buffer.extend_from_slice(&read_buffer[0..n]);
    }
    let mut headers = [httparse::EMPTY_HEADER; 16];
    let mut request = Request::new(&mut headers);
    request.parse(&buffer)?;
    log::info!("request text: {:?}", std::str::from_utf8(&buffer)?);
    log::info!("request: {:?}", request);
    parse_connect(&request)
}

fn is_partial_request(buffer: &[u8]) -> Result<bool> {
    let mut headers = [httparse::EMPTY_HEADER; 16];
    Ok(Request::new(&mut headers).parse(&buffer)?.is_partial())
}

async fn copy_and_log<'a, R, W>(
    name: &'static str,
    reader: &'a mut R,
    writer: &'a mut W,
) -> Result<()>
where
    R: AsyncRead + Unpin + ?Sized,
    W: AsyncWrite + Unpin + ?Sized,
{
    loop {
        let mut buffer = [0; 1024];
        let count = reader.read(&mut buffer).await?;
        if count == 0 {
            log::info!("{}: stream closed", name);
            // stream closed
            return Ok(());
        }
        log::info!("{}: {} bytes read", name, count);
        writer.write_all(&buffer[0..count]).await?;
    }
}

pub fn parse_connect<'headers, 'buf>(request: &Request) -> Result<(String, u16)> {
    if request.method != Some("CONNECT") {
        bail!("invalid method");
    }
    let path = if let Some(p) = request.path {
        p
    } else {
        bail!("invalid CONNECT: no path");
    };
    // TODO: find a robust parser
    let parts: Vec<_> = path.split(':').collect();
    if parts.len() > 2 {
        bail!("invalid CONNECT: malformed path");
    }
    let (host, port) = if parts.len() == 1 {
        (parts[0], 443)
    } else {
        (parts[0], parts[1].parse()?)
    };
    Ok((String::from(host), port))
}
