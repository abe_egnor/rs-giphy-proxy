use std::{collections::HashSet, net::SocketAddr, sync::Arc};

use anyhow::Result;
use tokio::net::TcpListener;

mod options;
mod request;
mod tls;

#[tokio::main]
async fn main() -> Result<()> {
    // Default to log-level INFO
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();

    let args: options::Args = argh::from_env();
    let domains: HashSet<String> = args.proxy_domains.split(':').map(String::from).collect();
    let domains = Arc::new(domains);

    let acceptor = tls::new_acceptor(&args.tls_cert_path, &args.tls_private_key_path).await?;
    let addr: SocketAddr = args.listen_addr.parse()?;
    let listener = TcpListener::bind(&addr).await?;

    log::info!("listening on {}", addr);
    loop {
        // TODO: handle accept errors rather than barfing
        let (stream, _) = listener.accept().await?;
        // Don't log client IP; timestamp could correlate with search.
        log::info!("spawning handler");
        let handler = request::Handler {
            acceptor: acceptor.clone(),
            domains: domains.clone(),
        };
        tokio::spawn(async move { handler.run(stream).await });
    }
}
